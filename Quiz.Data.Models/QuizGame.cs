﻿using System;
using System.Collections.Generic;
using System.Text;
using Quiz.Data.Common.Models;
using Quiz.Data.Models.Enums;

namespace Quiz.Data.Models
{
    public class QuizGame : BaseDeletableModel<int>
    {
        public string QuizName { get; set; }

        public List<Question> QuizQuestions { get; set; } = new List<Question>();

        public ApplicationMode Mode { get; set; }
    }
}
