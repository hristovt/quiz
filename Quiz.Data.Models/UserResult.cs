﻿using System;
using System.Collections.Generic;
using System.Text;
using Quiz.Data.Common.Models;

namespace Quiz.Data.Models
{
   public class UserResult : BaseDeletableModel<int>
    {       
        public string UserId { get; set; }

        public UserAccount User { get; set; }

        public int QuizId { get; set; }

        public QuizGame Quiz { get; set; }

        public int UsersCorrectAnswers { get; set; }

        public int UsersWrongAnswers { get; set; }
    }
}
