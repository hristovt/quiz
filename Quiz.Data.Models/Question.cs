﻿using System;
using System.Collections.Generic;
using System.Text;
using Quiz.Data.Common.Models;
using Quiz.Data.Models.Enums;

namespace Quiz.Data.Models
{
    public class Question : BaseDeletableModel<int>
    {
        public string QuestionName { get; set; }

        public string FirstAnswer { get; set; }

        public string SecondAnswer { get; set; }

        public string ThirdAnswer { get; set; }

        public CorrectAnswer CorrectAnswerIndex { get; set; }       

        public bool BinaryAnswer { get; set; }

        public bool MultipleChoiceMode { get; set; }

        public int QuizId { get; set; }

        public QuizGame Quiz { get; set; }
    }
}
