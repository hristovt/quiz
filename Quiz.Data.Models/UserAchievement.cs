﻿using System;
using System.Collections.Generic;
using System.Text;
using Quiz.Data.Common.Models;

namespace Quiz.Data.Models
{
    public class UserAchievement : BaseDeletableModel<int>
    {
        public string UserId { get; set; }

        public UserAccount User { get; set; }

        public int AchievementId { get; set; }

        public Achievement Achievement { get; set; }

        public DateTime AchievedOn { get; set; }
    }
}
