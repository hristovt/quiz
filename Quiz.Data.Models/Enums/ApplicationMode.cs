﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Quiz.Data.Models.Enums
{
    public enum ApplicationMode
    {
        Binary,
        Multiple
    }
}
