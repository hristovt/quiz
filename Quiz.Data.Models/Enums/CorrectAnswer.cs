﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz.Data.Models.Enums
{
    public enum CorrectAnswer
    {
        First = 0,
        Second = 1,
        Third = 2,
        None = 3
    }
}
