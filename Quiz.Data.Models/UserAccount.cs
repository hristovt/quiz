﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Identity;
using Quiz.Data.Common.Models;
using Quiz.Data.Models.Enums;

namespace Quiz.Data.Models
{
    public partial class UserAccount :  IdentityUser
    {
        public int UserId { get; set; }

        public string Name { get; set; }

        public bool Disabled { get; set; }

        public ApplicationMode? Mode { get; set; }

        public List<UserAchievement> UserAchievements { get; set; } = new List<UserAchievement>();
    }
}
