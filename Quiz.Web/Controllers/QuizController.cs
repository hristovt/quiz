﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Quiz.Services.Interfaces;
using Quiz.Common.ViewModels;
using Quiz.Data.Models;
using AutoMapper;
using Quiz.Services.ServiceModels;
using Microsoft.AspNetCore.Identity;

namespace Quiz.Web.Controllers
{
    public class QuizController : Controller
    {
        private readonly IQuizService quizService;
        private readonly IMapper mapper;
        //private readonly UserManager<UserAccount> userManager;


        public QuizController(IQuizService quizService, IMapper mapper)
        {
            this.quizService = quizService;
            this.mapper = mapper;
            //this.userManager = userManager;
        }

        [HttpGet]
        public IActionResult CreateQuiz()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> CreateQuiz(CreateQuizBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return this.View();
            }

            var quiz = mapper.Map<QuizGame>(model);

            await this.quizService.CreateQuiz(quiz);

            return RedirectToAction("Index", "Main");
        }
        
        public async Task<IActionResult> DeleteQuiz(int id)
        {
            await this.quizService.DeleteQuiz(id);

            return RedirectToAction("Index", "Main");
        }

        [HttpGet]
        public async Task<IActionResult> StartQuiz(int id)
        {
            var getQuiz = await this.quizService.GetQuizById(id);
            var jsonData = await this.quizService.GetJsonQuizById(id);

            var quiz = mapper.Map<QuizViewModel>(getQuiz);

            if (quiz == null)
            {
                return this.View();
            }
            ViewData["jsonData"] = jsonData;
            return this.View(quiz);
        }

        [HttpGet]
        public IActionResult Settings()
        {            
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Settings(UserBindingModel model)
        {
            //var currentUser = await userManager.GetUserAsync(User);

            if (!ModelState.IsValid)
            {
                return this.View();
            }
            //currentUser.Mode = model.Mode;

            //await this.quizService.UserSettings(currentUser);

            return RedirectToAction("Index", "Main");
        }
    }
}
