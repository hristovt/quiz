﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Quiz.Common.ViewModels;
using Quiz.Services.Interfaces;
using Quiz.Web.Models;
using ReflectionIT.Mvc.Paging;

namespace Quiz.Web.Controllers
{
    public class MainController : Controller
    {
        private readonly ILogger<MainController> _logger;
        private readonly IMapper mapper;
        private readonly IQuizService quizService;

        public MainController(ILogger<MainController> logger, IMapper mapper, IQuizService quizService)
        {
            this.mapper = mapper;
            this.quizService = quizService;
            _logger = logger;
        }


        public async Task<IActionResult> Index(int page = 1)
        {
            var quizzes = (await this.quizService.AllQuizzes())
                .Select(mapper.Map<AllQuizzesViewModel>);

            var pagedQuizzes = PagingList.Create(quizzes, 8, page);

            return this.View(pagedQuizzes);
        }


        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
