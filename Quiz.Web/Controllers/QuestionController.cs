﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Quiz.Common.ViewModels;
using Quiz.Data.Models;
using Quiz.Services.Interfaces;

namespace Quiz.Web.Controllers
{
    public class QuestionController : Controller
    {
        private readonly IQuestionService questionService;
        private readonly IQuizService quizService;
        private readonly IMapper mapper;

        public QuestionController(IQuestionService questionService, IQuizService quizService, IMapper mapper)
        {
            this.questionService = questionService;
            this.quizService = quizService;
            this.mapper = mapper;
        }

        [HttpGet]
        public async Task<IActionResult> AddQuestion()
        {
            var quizzes = (await this.quizService.AllQuizzes())
                .Select(mapper.Map<AllQuizzesViewModel>);

            ViewData["Quizzes"] = new SelectList(quizzes, "Id", "QuizName");

            return this.View();
        }

        [HttpPost]
        public async Task<IActionResult> AddQuestion(AddQuestionBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return this.View();
            }

            var question = mapper.Map<Question>(model);

            await this.questionService.AddQuestion(question);

            return RedirectToAction("Index", "Main", new { id = model.QuizId });
        }
    }
}
