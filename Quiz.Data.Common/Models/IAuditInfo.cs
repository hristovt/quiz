﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Quiz.Data.Common.Models
{
    public interface IAuditInfo
    {
        DateTime CreatedOn { get; set; }
        DateTime? ModifiedOn { get; set; }
    }
}
