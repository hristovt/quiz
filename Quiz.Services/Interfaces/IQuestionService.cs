﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Quiz.Data.Models;

namespace Quiz.Services.Interfaces
{
    public interface IQuestionService
    {
        Task AddQuestion(Question question);
    }
}
