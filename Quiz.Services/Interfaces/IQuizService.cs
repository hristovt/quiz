﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Quiz.Data.Models;
using Quiz.Services.ServiceModels;

namespace Quiz.Services.Interfaces
{
    public interface IQuizService
    {
        Task CreateQuiz(QuizGame quiz);

        Task DeleteQuiz(int id);

        Task<IEnumerable<QuizGame>> AllQuizzes();

        Task<QuizGame> GetQuizById(int id);

        Task<string> GetJsonQuizById(int id);

        Task StartQuiz(QuizServiceModel model, string username);

        Task UserSettings(UserAccount user);
    }
}
