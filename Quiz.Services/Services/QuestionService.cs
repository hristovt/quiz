﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Quiz.Data;
using Quiz.Data.Models;
using Quiz.Services.Interfaces;

namespace Quiz.Services.Services
{
    public class QuestionService : DataService, IQuestionService
    {
        public QuestionService(QuizDbContext context) : base(context)
        {
        }
        public async Task AddQuestion(Question question)
        {
            question.CreatedOn = DateTime.Now;
            await this.context.Questions.AddAsync(question);

            await this.context.SaveChangesAsync();
        }
    }
}
