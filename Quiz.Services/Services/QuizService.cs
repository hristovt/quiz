﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text.Json;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Quiz.Common.ViewModels;
using Quiz.Data;
using Quiz.Data.Models;
using Quiz.Services.Interfaces;
using Quiz.Services.ServiceModels;
using static Quiz.Common.ViewModels.StartQuizViewModel;

namespace Quiz.Services.Services
{
    public class QuizService : DataService, IQuizService
    {
        private readonly IMapper mapper;
        public QuizService(QuizDbContext context, IMapper mapper) : base(context)
        {
            this.mapper = mapper;
        }

        public async Task CreateQuiz(QuizGame quiz)
        {
            quiz.CreatedOn = DateTime.Now;
            await this.context.Quizzes.AddAsync(quiz);
            await this.context.SaveChangesAsync();
        }

        public async Task DeleteQuiz(int id)
        {
            var quiz = await this.context.Quizzes.Include(q => q.QuizQuestions).FirstOrDefaultAsync(q => q.Id == id);

            if (quiz == null)
            {
                return;
            }

            this.context.Quizzes.Remove(quiz);
            this.context.Questions.RemoveRange(quiz.QuizQuestions);
            await this.context.SaveChangesAsync();

        }

        public async Task<IEnumerable<QuizGame>> AllQuizzes()
        {
            var quizzes = await this.context.Quizzes.Include(q => q.QuizQuestions).ToListAsync();

            return quizzes;
        }

        public async Task<string> GetJsonQuizById(int id)
        {
            var quiz = await this.context.Quizzes.Include(q => q.QuizQuestions).FirstOrDefaultAsync(q => q.Id == id);
            var correctAnswer = string.Empty;
            var questionModel = new StartQuestionViewModel();
            var questions = new List<StartQuestionViewModel>();
            int correctAnswerIndex = 3;

            foreach (var question in quiz.QuizQuestions)
            {
                switch ((int)question.CorrectAnswerIndex)
                {
                    case 0:
                        correctAnswer = question.FirstAnswer;
                        break;
                    case 1:
                        correctAnswer = question.FirstAnswer;
                        break;
                    case 2:
                        correctAnswer = question.FirstAnswer;
                        break;
                }
                correctAnswerIndex = (int)question.CorrectAnswerIndex;
                questionModel.q = question.QuestionName;
                questionModel.options.Add(!string.IsNullOrWhiteSpace(question.FirstAnswer) ? question.FirstAnswer : string.Empty);
                questionModel.options.Add(!string.IsNullOrWhiteSpace(question.FirstAnswer) ? question.SecondAnswer : string.Empty);
                questionModel.options.Add(!string.IsNullOrWhiteSpace(question.FirstAnswer) ? question.ThirdAnswer : string.Empty);
                questionModel.correctIndex = correctAnswerIndex.ToString();
                questionModel.correctResponse = string.Format("Correct! The right answer is: {0}", correctAnswer);
                questionModel.incorrectResponse = string.Format("Sorry, you are wrong! The right answer is: {0}", correctAnswer);
                questions.Add(questionModel);
            }

            string questionsJson = System.Text.Json.JsonSerializer.Serialize(
                questions, 
                typeof(List<StartQuestionViewModel>), 
                new JsonSerializerOptions() { Encoder = System.Text.Encodings.Web.JavaScriptEncoder.Create(System.Text.Unicode.UnicodeRanges.All) })
                .Replace("\"", "\'");
            return questionsJson;
        }

        public async Task<QuizGame> GetQuizById(int id)
        {
            var quiz = await this.context.Quizzes.Include(q => q.QuizQuestions).FirstOrDefaultAsync(q => q.Id == id);

            return quiz;
        }

        public async Task StartQuiz(QuizServiceModel model, string username)
        {
            var user = await this.context.Users.FirstOrDefaultAsync(u => u.UserName == username);

            var quiz = await this.context.Quizzes.Include(q => q.QuizQuestions).FirstOrDefaultAsync(q => q.Id == model.Id);

            if (user == null || quiz == null)
            {
                return;
            }

            var results = new UserResult();
            results.UserId = user.Id;
            results.User = (UserAccount)user;
            results.QuizId = quiz.Id;
            results.Quiz = quiz;

            model.Result = results;

            for (int i = 0; i < quiz.QuizQuestions.Count; i++)
            {
                var questionId = quiz.QuizQuestions[i].Id;
                var correctAnswer = quiz.QuizQuestions[i].CorrectAnswerIndex;
                var currentAnswer = model.Answers.FirstOrDefault(x => x.QuestionId == questionId).Answer;
            }

            await this.context.UserResults.AddAsync(model.Result);
            await this.context.SaveChangesAsync();
        }

        public async Task UserSettings(UserAccount user)
        {
            await this.context.UserAccounts.AddAsync(user);
            await this.context.SaveChangesAsync();
        }

    }
}
