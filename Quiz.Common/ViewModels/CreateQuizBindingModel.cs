﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Quiz.Data.Models.Enums;

namespace Quiz.Common.ViewModels
{
   public class CreateQuizBindingModel
    {
        [Required(ErrorMessage = "Name is required and must be maximum 20 symbols!")]
        [StringLength(20)]
        public string QuizName { get; set; }

        [Required(ErrorMessage = "Select category is required")]
        public ApplicationMode Mode { get; set; }
    }
}
