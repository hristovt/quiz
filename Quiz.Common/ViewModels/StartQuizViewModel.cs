﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Quiz.Common.ViewModels
{
    public class StartQuizViewModel
    {
        private string counterFormat;
        private List<StartQuestionViewModel> questions;

        [JsonProperty("counterFormat")]
        public string CounterFormat { get => counterFormat; set => counterFormat = value; }

        [JsonProperty("questions")]
        public List<StartQuestionViewModel> Questions { get => questions; set => questions = value; }       
    }
}
