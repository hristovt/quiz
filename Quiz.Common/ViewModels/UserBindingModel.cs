﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Quiz.Data.Models.Enums;

namespace Quiz.Common.ViewModels
{
    public class UserBindingModel
    {
        [Required(ErrorMessage = "Select mode is required")]
        public ApplicationMode Mode { get; set; }
    }
}
