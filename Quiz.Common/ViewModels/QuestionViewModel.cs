﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Quiz.Data.Models.Enums;

namespace Quiz.Common.ViewModels
{
   public class QuestionViewModel
    {
        public int Id { get; set; }

        public string QuestionName { get; set; }

        public string FirstAnswer { get; set; }

        public string SecondAnswer { get; set; }

        public string ThirdAnswer { get; set; }

        public CorrectAnswer CorrectAnswerIndex { get; set; }

        public bool BinaryAnswer { get; set; }

        public bool MultipleChoiceMode { get; set; }
    }
}
