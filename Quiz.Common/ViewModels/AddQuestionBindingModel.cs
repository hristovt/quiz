﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Quiz.Data.Models;
using Quiz.Data.Models.Enums;

namespace Quiz.Common.ViewModels
{
   public class AddQuestionBindingModel
    {
        [Required(ErrorMessage = "Question name is required and must be maximum 100 symbols!")]
        [StringLength(100)]
        public string QuestionName { get; set; }

        
        [StringLength(100)]
        public string FirstAnswer { get; set; }

        
        [StringLength(100)]
        public string SecondAnswer { get; set; }

       
        [StringLength(100)]
        public string ThirdAnswer { get; set; }
       
        public CorrectAnswer CorrectAnswerIndex { get; set; }

        public bool BinaryAnswer { get; set; }

        public bool MultipleChoiceMode { get; set; }

        public int QuizId { get; set; }

        public QuizGame Quiz { get; set; }

        public IEnumerable<QuizGame> Quizzes { get; set; }
    }
}
