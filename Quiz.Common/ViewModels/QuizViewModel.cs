﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Quiz.Data.Models.Enums;

namespace Quiz.Common.ViewModels
{
    public class QuizViewModel
    {
        public int Id { get; set; }

        public string QuizName { get; set; }

        public ApplicationMode Mode { get; set; }

        public string QuizLogoUrl { get; set; }

        public ICollection<QuestionViewModel> QuizQuestions { get; set; }


        public List<AnswersBindingModel> Answers { get; set; }
    }
}
