﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Quiz.Common.ViewModels
{
    public class StartQuestionViewModel
    {
        private List<string> questionOptions;
        public StartQuestionViewModel()
        {
            questionOptions = new List<string>();
        }

        [JsonProperty("q")]
        public string q { get; set; }

        [JsonProperty("options")]
        public List<string> options { get => questionOptions; set => questionOptions = value; }

        [JsonProperty("correctIndex")]
        public string correctIndex { get; set; }

        [JsonProperty("correctResponse")]
        public string correctResponse { get; set; }

        [JsonProperty("incorrectResponse")]
        public string incorrectResponse { get; set; }
    }
}
