﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz.Common.ViewModels
{
    public class AnswersBindingModel
    {
        public int QuestionId { get; set; }
        public string Answer { get; set; }
    }
}
