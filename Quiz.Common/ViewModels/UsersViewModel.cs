﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Quiz.Data.Models;
using Quiz.Data.Models.Enums;

namespace Quiz.Common.ViewModels
{
    class UsersViewModel
    {
        public int UserId { get; set; }

        public string Name { get; set; }

        public bool Disabled { get; set; }

        public ApplicationMode Mode { get; set; }

        public List<UserAchievement> UserAchievements { get; set; } = new List<UserAchievement>();
    }
}
