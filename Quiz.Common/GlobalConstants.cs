﻿using System;

namespace Quiz.Common
{
    public static class GlobalConstants
    {
        public const string SystemName = "QuizHristoHristov";

        public const string AdministratorRoleName = "Administrator";
    }
}
