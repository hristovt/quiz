﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz.Services.ServiceModels
{
    public class AnswersServiceModel
    {
        public int QuestionId { get; set; }
        public string Answer { get; set; }
    }
}
