﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Quiz.Common.ViewModels;
using Quiz.Data.Models;
using Quiz.Services.ServiceModels;

namespace Quiz.Common.Mapper
{
    public class AutoMapperConfiguration : Profile
    {
        public AutoMapperConfiguration()
        {
            this.CreateMap<QuizGame, CreateQuizBindingModel>().ReverseMap();
            this.CreateMap<QuizGame, QuizViewModel>().ReverseMap();
            this.CreateMap<QuizServiceModel, QuizViewModel>().ReverseMap();
            this.CreateMap<Question, QuestionViewModel>().ReverseMap();
            this.CreateMap<AnswersServiceModel, AnswersBindingModel>().ReverseMap();
            this.CreateMap<QuizGame, AllQuizzesViewModel>().ReverseMap();
            this.CreateMap<Question, AddQuestionBindingModel>().ReverseMap();
            this.CreateMap<UserAccount, UserBindingModel>().ReverseMap();
            this.CreateMap<UserAccount, UsersViewModel>().ReverseMap();
            this.CreateMap<IdentityUser, UserAccount>().ReverseMap();
        }
    }
}
