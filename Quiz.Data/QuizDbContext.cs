﻿using System;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Quiz.Data.Models;

namespace Quiz.Data
{
    public class QuizDbContext : IdentityDbContext
    {
        public QuizDbContext(DbContextOptions<QuizDbContext> options)
            : base(options)
        {
        }
               

        public DbSet<QuizGame> Quizzes { get; set; }
        public DbSet<Models.Question> Questions { get; set; }

        public DbSet<UserAccount> UserAccounts { get; set; }

        public DbSet<UserResult> UserResults { get; set; }

        public DbSet<Achievement> Achievements { get; set; }

        public DbSet<UserAchievement> UserAchievements { get; set; }
    }
}
